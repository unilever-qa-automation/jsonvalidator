package json.validator.crawler;

import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.fge.jsonschema.core.report.ProcessingReport;

import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.crawler.WebCrawler;
import edu.uci.ics.crawler4j.parser.HtmlParseData;
import edu.uci.ics.crawler4j.url.WebURL;
import json.validator.common.Component;
import json.validator.common.ConfigConstants;
import json.validator.utils.JSONUtils;

public class Crawler extends WebCrawler {
	protected static final Logger logger = LoggerFactory.getLogger(Crawler.class);

	@Override
	public boolean shouldVisit(Page referringPage, WebURL url) {
		Matcher m = ConfigConstants.shouldVisitPattern.matcher(url.getURL());
		return !ConfigConstants.SKIPPED_URLS.contains(url.getModifiedHost()) && (m.find());
	}

	@Override
	public void visit(Page page) {
		if (page.getWebURL().isInternalLink() && page.getStatusCode() == 200
				&& page.getContentType().contains("text/html")) {
			Document document = Jsoup.parse(getHtml(page), ConfigConstants.site);
			List<Element> elements = document.select("div.component-wrapper.section");
			for (Element element : elements) {
				Component component = new Component();
				component.setUrl(page.getWebURL().getURL());
				for (Element node : element.select("div")) {
					if (node.hasClass("component") && node.hasAttr("data-role")) {
						component.setRole(node.attr("data-role"));
					} else if (!node.hasClass("component") && node.hasAttr("data-componentname")) {
						component.setName(node.attr("data-componentname"));
						if (node.hasAttr("data-component-positions"))
							component.setPosition(Integer.parseInt(node.attr("data-component-positions")));
						if (node.hasAttr("data-component-variants"))
							component.setVarient(node.attr("data-component-variants"));
					}
				}
				try {
					String json = element.select("script").first().text();
					String schema = JSONUtils.getJsonSchemaAsString(component.getRole());
					ProcessingReport report = JSONUtils.getJsonValidationResult(json, schema);
					component.setStatus(report.isSuccess());
					component.setResult(report);

				} catch (IOException e) {
					logger.error("Error: " + e);
				}
			}
		}
	}

	private String getHtml(Page page) {
		return page.getParseData() instanceof HtmlParseData ? ((HtmlParseData) page.getParseData()).getHtml() : null;
	}

}
