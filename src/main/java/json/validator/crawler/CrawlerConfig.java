package json.validator.crawler;

import java.net.MalformedURLException;

import org.slf4j.LoggerFactory;

import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.authentication.BasicAuthInfo;
import json.validator.common.ConfigConstants;

public class CrawlerConfig {

	public CrawlConfig getConfig() {
		CrawlConfig config = new CrawlConfig();
		config.setSiteUrl(ConfigConstants.site);
		if (null != ConfigConstants.user && !ConfigConstants.user.isEmpty()) {
			try {
				config.addAuthInfo(new BasicAuthInfo(ConfigConstants.user, ConfigConstants.pass, ConfigConstants.site));
			} catch (MalformedURLException e) {
				LoggerFactory.getLogger(CrawlerConfig.class).debug("Error in controller", e);
			}
		}
		config.setUserAgentString(ConfigConstants.USER_AGENT);
		config.setCrawlStorageFolder(ConfigConstants.crawlStorageFolder);
		config.setConnectionTimeout(
				Integer.parseInt(ConfigConstants.PROPERTIES.getProperty("crawler.connectionTimeout", "120000")));
		config.setSocketTimeout(
				Integer.parseInt(ConfigConstants.PROPERTIES.getProperty("crawler.connectionTimeout", "120000")));
		config.setFollowRedirects(
				Boolean.parseBoolean(ConfigConstants.PROPERTIES.getProperty("crawler.followRedirects", "true")));
		config.setPolitenessDelay(
				Integer.parseInt(ConfigConstants.PROPERTIES.getProperty("crawler.URLHitDelay", "200")));
		config.setProcessBinaryContentInCrawling(false);
		config.setIncludeBinaryContentInCrawling(false);
		config.setIncludeHttpsPages(true);
		config.setMaxDownloadSize(Integer.parseInt(ConfigConstants.PROPERTIES.getProperty("crawler.maxDownloadSize",
				Integer.toString(Integer.MAX_VALUE))));
		return config;
	}
}
