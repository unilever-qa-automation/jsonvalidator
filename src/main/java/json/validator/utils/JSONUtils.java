package json.validator.utils;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;

/***
 * 
 * @author sku202
 *
 */

public class JSONUtils {
	protected static final Logger logger = LoggerFactory.getLogger(JSONUtils.class);

	/***
	 * 
	 * @param json
	 *            json string to verify
	 * @param schema
	 *            json schema against which json will be validated
	 * @return ProcessingReport containing report
	 */

	public static ProcessingReport getJsonValidationResult(String json, String schema) {
		ProcessingReport report = null;
		JsonSchemaFactory factory = JsonSchemaFactory.byDefault();

		try {
			JsonNode schemaNode = JsonLoader.fromString(schema);
			JsonNode data = JsonLoader.fromString(json);
			JsonSchema jsonSchema = factory.getJsonSchema(schemaNode);
			report = jsonSchema.validateUnchecked(data, true);
		} catch (JsonParseException e) {
			logger.error("Error in parsing JSON: " + e);
		} catch (ProcessingException e) {
			logger.error("Error in processing: " + e);
		} catch (IOException e) {
			logger.error("Error in reading file: " + e);
		}
		return report;
	}

	/***
	 * 
	 * @param json
	 *            file containing json to verify
	 * @param schema
	 *            file containing json schema against which json will be
	 *            validated
	 * @return ProcessingReport containing report
	 */
	public static ProcessingReport getJsonValidationResult(File json, File schema) {
		ProcessingReport report = null;
		JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
		try {
			JsonNode schemaNode = JsonLoader.fromFile(schema);
			JsonNode data = JsonLoader.fromFile(json);
			JsonSchema jsonSchema = factory.getJsonSchema(schemaNode);
			report = jsonSchema.validateUnchecked(data, true);
		} catch (JsonParseException e) {
			logger.error("Error in parsing JSON: ", e);
		} catch (ProcessingException e) {
			logger.error("Error in processing: ", e);
		} catch (IOException e) {
			logger.error("Error in reading file: ", e);
		}
		return report;
	}

	/***
	 * 
	 * @param json
	 *            json as string to validate
	 * @return return true if json is valid. else false.
	 */
	public static boolean validateJSON(String json) {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			objectMapper.readTree(json);
			return true;
		} catch (JsonProcessingException e) {
			logger.error("Error in parsing JSON: " + e);
			return false;
		} catch (IOException e) {
			logger.error("IO Error in parsing JSON: " + e);
			return false;
		}
	}

	/***
	 * 
	 * @param json
	 *            file containing json to validate
	 * @return return true if json is valid. else false.
	 */
	public static boolean validateJSON(File json) {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			objectMapper.readTree(FileUtils.readFileToString(json, "utf-8"));
			return true;
		} catch (JsonProcessingException e) {
			logger.error("Error in parsing JSON: " + e);
			return false;
		} catch (IOException e) {
			logger.error("IO Error in parsing JSON: " + e);
			return false;
		}
	}

	/***
	 * 
	 * @param schemaName
	 *            schema name to be retrive
	 * @return schema in string format
	 * @throws IOException
	 */
	public static String getJsonSchemaAsString(String schemaName) throws IOException {
		return IOUtils.toString(ClassLoader.getSystemClassLoader().getResource("schemas/" + schemaName + ".json"),
				"utf-8");
	}

	/***
	 * 
	 * @param schemaName
	 *            schema name to be retrive
	 * @return schema in File object
	 * @throws IOException
	 */
	public static File getJsonSchemaAsFile(String schemaName) throws IOException {
		return new File(ClassLoader.getSystemClassLoader().getResource("schemas/" + schemaName + ".json").getFile());
	}
}
