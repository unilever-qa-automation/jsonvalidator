package json.validator.utils;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/***
 * 
 * @author sku202
 *
 */

public class HelperUtils {

    /**
     * Method returns the unique string based on time stamp
     *
     *
     * @return unique string
     */
    public static String generateUniqueString() {
	DateFormat df = new SimpleDateFormat("dd-MMMM-yyyy");
	DateFormat df1 = new SimpleDateFormat("hh-mm-ss-SSaa");
	Calendar calobj = Calendar.getInstance();
	String time = df1.format(calobj.getTime());
	String date = df.format(calobj.getTime());
	return date + "_" + time;
    }

    // public static String getResourceFile(String fileName) {
    // File file = null;
    // try {
    // // String str =
    // //
    // IOUtils.toString(HelperUtils.class.getClassLoader().getResourceAsStream(fileName));
    // String str = FileUtils.readFileToString(file, "utf-8");
    // file = new File(new File(ConfigConstants.dataLocation).getParentFile(),
    // fileName);
    // FileUtils.write(file, str, "utf-8");
    // } catch (IOException e) {
    // e.printStackTrace();
    // }
    // return file.getAbsolutePath();
    // }

    /**
     * 
     * @param fileName
     *            name of the file to be retrieved as
     * @param pROPERTIES_LOC
     *            file location to be read
     * @return modified file absolute path
     */
    public static String getResourceFile(String fileName, String pROPERTIES_LOC) {
	File file = null;
	try {
	    String str = FileUtils.readFileToString(new File(pROPERTIES_LOC), "UTF-8");
	    file = new File(System.getProperty("user.dir"), fileName);
	    FileUtils.write(file, str, "utf-8");
	} catch (IOException e) {
	    e.printStackTrace();
	}
	return file.getAbsolutePath();
    }

    /***
     * 
     * @param fileName
     *            relative path of file with respect to resources folder
     * @return file object of the file
     */
    public static File getResource(String fileName) {
	return new File(ClassLoader.getSystemClassLoader().getResource(fileName).getFile());
    }

    public static void generateReort(List<Map<String, String>> r) {
	File report = getResource("Report.html");
	try {
	    Document doc = Jsoup.parse(report, "utf-8");
	    Element element = doc.select("table tbody").first();
	    int i = 1;
	    for (Map<String, String> map : r) {
		String missing = map.get("missing") == null ? "----"
			: map.get("missing").replaceAll("[\\[\\]]", "").replaceAll("\",\"", "\", \"");
		String unwanted = map.get("unwanted") == null ? "----"
			: map.get("unwanted").replaceAll("[\\[\\]]", "").replaceAll("\",\"", "\", \"");
		String error = map.get("error") == null ? "----"
			: map.get("error").replaceAll("[\\[\\]]", "").replaceAll("\",\"", "\", \"");
		element.append("<tr><td>" + i++ + "</td><td>http://www.dove.com/asdasdad/dasdasda/asd3sdfs4asda</td><td>HeroImageV2</td><td>Hero-Image-V2</td><td>" + map.get("object")
			+ "</td><td>" + unwanted + "</td><td>" + missing + "</td><td>" + error + "</td></tr>");
	    }
	    FileUtils.write(new File("D:/report.html"), doc.toString(), "utf-8");
	} catch (IOException e) {
	    e.printStackTrace();
	}
    }
}
