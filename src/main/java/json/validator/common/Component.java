package json.validator.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.fge.jsonschema.core.report.ProcessingMessage;
import com.github.fge.jsonschema.core.report.ProcessingReport;
/***
 * 
 * @author sku202
 *
 */
public final class Component {
	private String name;
	private String url;
	private int position;
	private String varient;
	private String role;
	private List<Map<String, String>> result;
	private boolean status;

	public Component() {
	    result=new ArrayList<>();
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	protected static final Logger logger = LoggerFactory.getLogger(Component.class);

	public List<Map<String, String>> getResult() {
		return result;
	}

	public void setResult(ProcessingReport report) {
		Map<String, String> map = new HashMap<>();
		if (null != report) {
			for (ProcessingMessage message : report) {
				for (String s : message.toString().split("\n")) {
					map.put(s.substring(0, s.indexOf(":")).trim(), s.substring(s.indexOf(":") + 1).trim());
				}
			}
		} else {
			logger.debug("Report is null. Something went wrong.");
		}
		map.remove("schema");
		map.put("object", new JSONObject(map.remove("instance")).getString("pointer"));
		result.add(map);
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public String getVarient() {
		return varient;
	}

	public void setVarient(String varient) {
		this.varient = varient;
	}

	@Override
	public String toString() {
		return String.format("Component [url=%s, position=%s, role=%s]", url, position, role);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + position;
		result = prime * result + ((role == null) ? 0 : role.hashCode());
		result = prime * result + ((url == null) ? 0 : url.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Component other = (Component) obj;
		if (position != other.position)
			return false;
		if (role == null) {
			if (other.role != null)
				return false;
		} else if (!role.equals(other.role))
			return false;
		if (url == null) {
			if (other.url != null)
				return false;
		} else if (!url.equals(other.url))
			return false;
		return true;
	}

}
