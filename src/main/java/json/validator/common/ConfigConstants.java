package json.validator.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.regex.Pattern;

import org.slf4j.LoggerFactory;

import json.validator.utils.HelperUtils;
/***
 * 
 * @author sku202
 *
 */
public class ConfigConstants {
	public static final Pattern pattern;
	public static final Pattern shouldVisitPattern;
	public static final String REPORT_TIME_STAMP;
	public static final String MONGODB_HOST;
	public static final int MONGODB_PORT;
	public static final List<String> SKIPPED_URLS;
	public static final String crawlStorageFolder;
	public static final String dataLocation;
	public static final String USER_AGENT;
	public static final boolean caseSensitive;
	public static final String reportPath;
	public static final String outputDirectory;
	public static final String site;
	public static final String user;
	public static final String pass;
	public static final Properties PROPERTIES;
	// public static List<Map<String, String>> reportList = new
	// CopyOnWriteArrayList<>();

	static {
		site = System.getProperty("SiteAddress");
		user = System.getProperty("Username");
		pass = System.getProperty("Password");

		String host = "";
		try {
			host = new URL(site).getHost().replaceAll("www.", "");
		} catch (MalformedURLException e) {
			LoggerFactory.getLogger(ConfigConstants.class).debug("Error in loading config file", e);
		}
		REPORT_TIME_STAMP = HelperUtils.generateUniqueString();
		String outputDirectory1 = new File(System.getProperty("user.dir") + File.separator + "Reports" + File.separator
				+ host + File.separator + REPORT_TIME_STAMP).getAbsolutePath();
		if (null != System.getenv("JENKINS_URL") && !System.getenv("JENKINS_URL").isEmpty()) {
			outputDirectory = outputDirectory1.substring(0, outputDirectory1.indexOf("jenkins")) + File.separator
					+ "SEOBOX" + File.separator + "Reports";
		} else {
			outputDirectory = outputDirectory1;
		}
		reportPath = outputDirectory + File.separator + "Report.html";
		File storage = new File(System.getProperty("user.dir") + File.separator + "temp");
		storage.mkdirs();
		crawlStorageFolder = storage.getAbsolutePath();
		dataLocation = crawlStorageFolder + File.separator + host + File.separator + "urls";
		new File(dataLocation).mkdirs();
		PROPERTIES = new Properties();
		SKIPPED_URLS = new ArrayList<>();
		String PROPERTIES_LOC = "CrawlerConfigFile";
		try {
			if (!new File(PROPERTIES_LOC).exists()) {
				LoggerFactory.getLogger(ConfigConstants.class).info("Loading default config file");
				PROPERTIES_LOC = HelperUtils.getResource("Config.properties").getAbsolutePath();
			} else {
				LoggerFactory.getLogger(ConfigConstants.class).info("Loading user's config file");
				PROPERTIES_LOC = HelperUtils.getResourceFile("Config.properties", PROPERTIES_LOC);
			}
			FileInputStream in = new FileInputStream(new File(PROPERTIES_LOC));
			PROPERTIES.load(in);
			in.close();
			String[] sites = PROPERTIES.getProperty("crawler.skipDomains").split(",");
			for (String site : sites) {
				SKIPPED_URLS.add(new URL(site).getHost().replaceAll("www.", ""));
			}
		} catch (IOException e) {
			LoggerFactory.getLogger(ConfigConstants.class).debug("Error in loading config file", e);
		}
		pattern = Pattern.compile(PROPERTIES.getProperty("crawler.domainRegex", "."), Pattern.CASE_INSENSITIVE);
		shouldVisitPattern = Pattern.compile(PROPERTIES.getProperty("crawler.linksToVisit", "."),
				Pattern.CASE_INSENSITIVE);
		USER_AGENT = ConfigConstants.PROPERTIES.getProperty("crawler.userAgentString",
				"Mozilla/5.0 (Windows NT 10.0; WOW64; rv:48.0) Gecko/20100101 Firefox/48.0");
		MONGODB_HOST = PROPERTIES.getProperty("mongodb.host", "10.207.61.56");
		MONGODB_PORT = Integer.parseInt(PROPERTIES.getProperty("mongodb.port", "27017"));
		caseSensitive = Boolean.parseBoolean(PROPERTIES.getProperty("crawler.caseSensitiveUrl", "false"));
	}
}
