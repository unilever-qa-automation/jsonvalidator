package json.validator.common;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.fge.jsonschema.core.report.ProcessingMessage;
import com.github.fge.jsonschema.core.report.ProcessingReport;

import json.validator.utils.HelperUtils;
import json.validator.utils.JSONUtils;

/***
 * 
 * @author sku202
 *
 */

public class Demo {
    protected static final Logger logger = LoggerFactory.getLogger(Demo.class);

    public static void main(String[] args) {
	List<Map<String, String>> r = new ArrayList<>();
	try {
	    File json = new File("HeroV2Image.json");
	    File schema = JSONUtils.getJsonSchemaAsFile("HeroV2Image");
	    ProcessingReport report = JSONUtils.getJsonValidationResult(json, schema);
	    if (null != report) {
		for (ProcessingMessage message : report) {
		    Map<String, String> result = new HashMap<>();
		    for (String s : message.toString().split("\n")) {
			result.put(s.substring(0, s.indexOf(":")).trim(), s.substring(s.indexOf(":") + 1).trim());
		    }
		    result.remove("schema");
		    result.put("object", new JSONObject(result.remove("instance")).getString("pointer"));
		    r.add(result);
		}
	    } else {
		logger.error("Report is null. Something went wrong.");
	    }
	} catch (IOException e) {
	    e.printStackTrace();
	}
	HelperUtils.generateReort(r);
    }

}
